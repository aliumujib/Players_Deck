package com.quickblox.q_municate.ui.fragments.carddeck;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.quickblox.q_municate.R;
import com.quickblox.q_municate.ui.fragments.base.BaseLoaderFragment;
import com.quickblox.q_municate_db.models.Dialog;

import java.util.ArrayList;
import java.util.List;

import link.fls.swipestack.SwipeStack;

/**
 * Created by Abdul-Mujeeb Aliu on 9/20/2016.
 */
public class HomeFragment extends BaseLoaderFragment<List<Dialog>> implements SwipeStack.SwipeStackListener {
    private Context mContext;
    private View view;
    private SwipeStack mSwipeStack;
    private ArrayList<String> mData;
    private SwipeStackAdapter mAdapter;

    private void fillWithTestData() {
        for (int x = 0; x < 5; x++) {
            mData.add(getString(R.string.dummy_text) + " " + (x + 1));
        }
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }


    @Override
    protected Loader<List<Dialog>> createDataLoader() {
        return null;
    }

    @Override
    public void onLoadFinished(Loader<List<Dialog>> loader, List<Dialog> data) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mContext = getContext();
        view = inflater.inflate(R.layout.fragment_home_fragment, container, false);

        mSwipeStack = (SwipeStack) view.findViewById(R.id.swipeStack);
        mData = new ArrayList<>();
        mAdapter = new SwipeStackAdapter(mData);
        mSwipeStack.setAdapter(mAdapter);
        mSwipeStack.setListener(this);
        fillWithTestData();
        return view;
    }


    @Override
    public void onViewSwipedToRight(int position) {
        String swipedElement = mAdapter.getItem(position);

    }

    @Override
    public void onViewSwipedToLeft(int position) {
        String swipedElement = mAdapter.getItem(position);

    }

    @Override
    public void onStackEmpty() {
        Toast.makeText(getContext(), R.string.stack_empty, Toast.LENGTH_SHORT).show();
    }

    public class SwipeStackAdapter extends BaseAdapter {

        private ArrayList<String> mData;

        public SwipeStackAdapter(ArrayList<String> data) {
            this.mData = data;
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public String getItem(int position) {
            return mData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.card, parent, false);
            }

            TextView textViewCard = (TextView) convertView.findViewById(R.id.textViewCard);
            textViewCard.setText(mData.get(position));

            return convertView;
        }
    }
}

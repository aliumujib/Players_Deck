// Generated code from Butter Knife. Do not modify!
package com.quickblox.q_municate.ui.activities.authorization;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class LandingActivity$$ViewBinder<T extends com.quickblox.q_municate.ui.activities.authorization.LandingActivity> extends com.quickblox.q_municate.ui.activities.authorization.BaseAuthActivity$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131558547, "field 'appVersionTextView'");
    target.appVersionTextView = finder.castView(view, 2131558547, "field 'appVersionTextView'");
    view = finder.findRequiredView(source, 2131558582, "method 'login'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.login(p0);
        }
      });
    view = finder.findRequiredView(source, 2131558580, "method 'facebookConnect'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.facebookConnect(p0);
        }
      });
    view = finder.findRequiredView(source, 2131558579, "method 'signUp'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.signUp(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.appVersionTextView = null;
  }
}

// Generated code from Butter Knife. Do not modify!
package com.quickblox.q_municate.ui.adapters.search;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class LocalSearchAdapter$ViewHolder$$ViewBinder<T extends com.quickblox.q_municate.ui.adapters.search.LocalSearchAdapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558571, "field 'avatarImageView'");
    target.avatarImageView = finder.castView(view, 2131558571, "field 'avatarImageView'");
    view = finder.findRequiredView(source, 2131558721, "field 'titleTextView'");
    target.titleTextView = finder.castView(view, 2131558721, "field 'titleTextView'");
    view = finder.findRequiredView(source, 2131558712, "field 'labelTextView'");
    target.labelTextView = finder.castView(view, 2131558712, "field 'labelTextView'");
  }

  @Override public void unbind(T target) {
    target.avatarImageView = null;
    target.titleTextView = null;
    target.labelTextView = null;
  }
}

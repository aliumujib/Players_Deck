// Generated code from Butter Knife. Do not modify!
package com.quickblox.q_municate.ui.fragments.search;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class SearchFragment$$ViewBinder<T extends com.quickblox.q_municate.ui.fragments.search.SearchFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558704, "field 'searchViewPager'");
    target.searchViewPager = finder.castView(view, 2131558704, "field 'searchViewPager'");
    view = finder.findRequiredView(source, 2131558701, "field 'searchRadioGroup'");
    target.searchRadioGroup = finder.castView(view, 2131558701, "field 'searchRadioGroup'");
  }

  @Override public void unbind(T target) {
    target.searchViewPager = null;
    target.searchRadioGroup = null;
  }
}

// Generated code from Butter Knife. Do not modify!
package com.quickblox.q_municate.ui.activities.settings;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class SettingsActivity$$ViewBinder<T extends com.quickblox.q_municate.ui.activities.settings.SettingsActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558571, "field 'avatarImageView'");
    target.avatarImageView = finder.castView(view, 2131558571, "field 'avatarImageView'");
    view = finder.findRequiredView(source, 2131558595, "field 'fullNameTextView'");
    target.fullNameTextView = finder.castView(view, 2131558595, "field 'fullNameTextView'");
    view = finder.findRequiredView(source, 2131558601, "field 'pushNotificationSwitch' and method 'enablePushNotification'");
    target.pushNotificationSwitch = finder.castView(view, 2131558601, "field 'pushNotificationSwitch'");
    ((android.widget.CompoundButton) view).setOnCheckedChangeListener(
      new android.widget.CompoundButton.OnCheckedChangeListener() {
        @Override public void onCheckedChanged(
          android.widget.CompoundButton p0,
          boolean p1
        ) {
          target.enablePushNotification(p1);
        }
      });
    view = finder.findRequiredView(source, 2131558607, "field 'changePasswordView'");
    target.changePasswordView = finder.castView(view, 2131558607, "field 'changePasswordView'");
    view = finder.findRequiredView(source, 2131558599, "method 'editProfile'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.editProfile();
        }
      });
    view = finder.findRequiredView(source, 2131558603, "method 'inviteFriends'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.inviteFriends();
        }
      });
    view = finder.findRequiredView(source, 2131558605, "method 'giveFeedback'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.giveFeedback();
        }
      });
    view = finder.findRequiredView(source, 2131558608, "method 'changePassword'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.changePassword();
        }
      });
    view = finder.findRequiredView(source, 2131558609, "method 'logout'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.logout();
        }
      });
    view = finder.findRequiredView(source, 2131558611, "method 'deleteAccount'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.deleteAccount();
        }
      });
  }

  @Override public void unbind(T target) {
    target.avatarImageView = null;
    target.fullNameTextView = null;
    target.pushNotificationSwitch = null;
    target.changePasswordView = null;
  }
}

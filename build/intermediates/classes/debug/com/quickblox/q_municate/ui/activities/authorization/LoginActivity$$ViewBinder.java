// Generated code from Butter Knife. Do not modify!
package com.quickblox.q_municate.ui.activities.authorization;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class LoginActivity$$ViewBinder<T extends com.quickblox.q_municate.ui.activities.authorization.LoginActivity> extends com.quickblox.q_municate.ui.activities.authorization.BaseAuthActivity$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131558588, "field 'rememberMeSwitch' and method 'rememberMeCheckedChanged'");
    target.rememberMeSwitch = finder.castView(view, 2131558588, "field 'rememberMeSwitch'");
    ((android.widget.CompoundButton) view).setOnCheckedChangeListener(
      new android.widget.CompoundButton.OnCheckedChangeListener() {
        @Override public void onCheckedChanged(
          android.widget.CompoundButton p0,
          boolean p1
        ) {
          target.rememberMeCheckedChanged(p1);
        }
      });
    view = finder.findRequiredView(source, 2131558589, "method 'forgotPassword'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.forgotPassword(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.rememberMeSwitch = null;
  }
}

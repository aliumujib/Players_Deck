// Generated code from Butter Knife. Do not modify!
package com.quickblox.q_municate.ui.adapters.chats;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class BaseDialogMessagesAdapter$ViewHolder$$ViewBinder<T extends com.quickblox.q_municate.ui.adapters.chats.BaseDialogMessagesAdapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findOptionalView(source, 2131558571, null);
    target.avatarImageView = finder.castView(view, 2131558571, "field 'avatarImageView'");
    view = finder.findOptionalView(source, 2131558572, null);
    target.nameTextView = finder.castView(view, 2131558572, "field 'nameTextView'");
    view = finder.findOptionalView(source, 2131558719, null);
    target.textMessageView = view;
    view = finder.findOptionalView(source, 2131558784, null);
    target.messageDeliveryStatusImageView = finder.castView(view, 2131558784, "field 'messageDeliveryStatusImageView'");
    view = finder.findOptionalView(source, 2131558780, null);
    target.attachDeliveryStatusImageView = finder.castView(view, 2131558780, "field 'attachDeliveryStatusImageView'");
    view = finder.findOptionalView(source, 2131558781, null);
    target.progressRelativeLayout = finder.castView(view, 2131558781, "field 'progressRelativeLayout'");
    view = finder.findOptionalView(source, 2131558777, null);
    target.attachMessageRelativeLayout = finder.castView(view, 2131558777, "field 'attachMessageRelativeLayout'");
    view = finder.findOptionalView(source, 2131558715, null);
    target.messageTextView = finder.castView(view, 2131558715, "field 'messageTextView'");
    view = finder.findOptionalView(source, 2131558778, null);
    target.attachImageView = finder.castView(view, 2131558778, "field 'attachImageView'");
    view = finder.findOptionalView(source, 2131558714, null);
    target.timeTextMessageTextView = finder.castView(view, 2131558714, "field 'timeTextMessageTextView'");
    view = finder.findOptionalView(source, 2131558779, null);
    target.timeAttachMessageTextView = finder.castView(view, 2131558779, "field 'timeAttachMessageTextView'");
    view = finder.findOptionalView(source, 2131558782, null);
    target.verticalProgressBar = finder.castView(view, 2131558782, "field 'verticalProgressBar'");
    view = finder.findOptionalView(source, 2131558783, null);
    target.centeredProgressBar = finder.castView(view, 2131558783, "field 'centeredProgressBar'");
    view = finder.findOptionalView(source, 2131558718, null);
    target.acceptFriendImageView = finder.castView(view, 2131558718, "field 'acceptFriendImageView'");
    view = finder.findOptionalView(source, 2131558717, null);
    target.dividerView = view;
    view = finder.findOptionalView(source, 2131558716, null);
    target.rejectFriendImageView = finder.castView(view, 2131558716, "field 'rejectFriendImageView'");
  }

  @Override public void unbind(T target) {
    target.avatarImageView = null;
    target.nameTextView = null;
    target.textMessageView = null;
    target.messageDeliveryStatusImageView = null;
    target.attachDeliveryStatusImageView = null;
    target.progressRelativeLayout = null;
    target.attachMessageRelativeLayout = null;
    target.messageTextView = null;
    target.attachImageView = null;
    target.timeTextMessageTextView = null;
    target.timeAttachMessageTextView = null;
    target.verticalProgressBar = null;
    target.centeredProgressBar = null;
    target.acceptFriendImageView = null;
    target.dividerView = null;
    target.rejectFriendImageView = null;
  }
}
